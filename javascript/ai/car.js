function Car(car, predictor, turnOptimalizator) {
    this.car = car;
    this.carData = null;
    this.trackInfo = null;
    this.currentLane = 0;
    this.predictor = predictor;
    this.turnOptimizer = turnOptimalizator ;
    this.currentPieceIndex = 0;
    this.currentSpeed = 0;
    this.inPieceDistance = 0;
    this.isSwitching = false;
    this.optimalSwitching = 0;
    this.lastPieceIndex = null;
    this.braking = false;

    this.previousPieceDistance = 0;
    this.previousPieceIndex = 0;
    this.previousAngle = 0;

    this.turboEnd = 0;
    this.turboFactor = 0;
    this.turboTicks = 0;
    this.turboEnabled = false;
    this.turboAvailable = false;

    this.state = {};
}

//powinna być wywoływana na początku, po każdym komunikacie carPositions
Car.prototype.update = function (carsData, tick) {
    for (var i = 0; i < carsData.length; i++) {
        var carData = carsData[i];

        if (carData.id.color == this.car.color) {
            this.carData = carData;
            this.angle = carData.angle
            this.currentLane = carData.piecePosition.lane.endLaneIndex;
            this.currentPieceIndex = carData.piecePosition.pieceIndex;
            this.inPieceDistance = carData.piecePosition.inPieceDistance;
            this.isSwitching = carData.piecePosition.lane.startLaneIndex != carData.piecePosition.lane.endLaneIndex;
            this.state = {
                'tick': tick,
                'angle': this.angle,
                'lane': this.currentLane,
                'pieceIndex': this.currentPieceIndex,
                'inPieceDistance': this.inPieceDistance,
                'currentSpeed': this.currentSpeed,
                'turboFactor': this.turboFactor,
                'turboEnabled': this.turboEnabled,
                'isSwitching': this.isSwitching
            };
        }
    }

    this.countCurrentSpeed();
};

//musi być wywoływane przy każdym końcu okrążenia
Car.prototype.lapFinished = function (lapData) {
    console.log(lapData);
    if (lapData.car.color == this.car.color) {
        this.trackInfo.currentLap++;
        console.log(this.trackInfo.currentLap);
        console.log(this.trackInfo.laps);
        if (this.trackInfo.currentLap == this.trackInfo.laps) {
            console.log('ostatnie okrazenie')
        }
    }

    console.log('===========[ LAP : ' + this.trackInfo.currentLap + ' : ' + (lapData.lapTime.millis/1000)  + 's ]=============');
    console.log(this.turnOptimizer.data);
};

var lastpiece = null;
Car.prototype.getThrottle = function(turnsPiece) {
    if (lastpiece !== null && lastpiece.start !== turnsPiece.start) {
        console.log(this.turnOptimizer.data);
    }
    lastpiece = turnsPiece;

    //console.log(turnsPiece.start + ' (' +Math.round(turnsPiece.progress*100)/100 + '), speed: '+ this.currentSpeed + ' ang: ' + this.state.angle);
    if (this.turboAvailable && turnsPiece.next.turboPiece
        && turnsPiece.outTurnZone) {
            this.turboAvailable = false;
            return 'turbo';
    } else if (turnsPiece.inTurnZone
        && !(!turnsPiece.next.isTurn && turnsPiece.outTurnZone)
        && !(turnsPiece.next.isTurn && turnsPiece.outTurnZone
        && turnsPiece.next.smoothness < turnsPiece.smoothness)) {
        //zakret
        return this.turnOptimizer.getThrottle(turnsPiece, this.state);
    }

    //ostatnia prosta
    if (turnsPiece.start > turnsPiece.end
        && this.pieceIndex > turnsPiece.start
        && this.trackInfo.currentLap == this.trackInfo.laps) {
        return 1;
    }

    //prosta
    //poczatek lub koniec zakretu
    var straightDistance = this.trackInfo.getNextHardTurnDistance(turnsPiece, this.state);
    var nextTurn = this.trackInfo.getNextTurn(turnsPiece);

    console.log('nextTurn: ' + nextTurn.start);
    var speed = this.turnOptimizer.getSpeedForTurn(nextTurn, this.state.lane);

    if (this.lastPieceIndex === null || this.state.pieceIndex != this.lastPieceIndex) {
        this.lastPieceIndex = this.state.pieceIndex;
    }
    var brakingDistance = this.predictor.predictBraking(this.currentSpeed, speed);
    if (straightDistance < brakingDistance) {
        if (!this.braking) {
            this.braking = true;
        }
        return 0;
    }
    if (this.braking) {
        this.braking = false;
    }
    return 1.0;
};

Car.prototype.countCurrentSpeed = function () {
    if (this.currentPieceIndex != this.previousPieceIndex) {
        var previousPiece = this.trackInfo.track.pieces[this.previousPieceIndex];
        this.currentSpeed = this.inPieceDistance
            + (this.trackInfo.getLengthOfPiece(previousPiece, this.trackInfo.track.lanes[this.currentLane].distanceFromCenter) - this.previousPieceDistance);
        this.previousPieceIndex = this.currentPieceIndex;
    }
    else {
        this.currentSpeed = this.inPieceDistance - this.previousPieceDistance;
    }
    this.previousPieceDistance = this.inPieceDistance;
};

module.exports = Car;