function TrackInfo(track, laps) {
    this.track = clone(track);
    this.laps = laps;
    this.currentLap = 1;

    this.lanes = track['lanes'];
    this.trackSplits = {};
    this.trackSplits.turns = this.splitTrackByTurns(track);
    this.trackSplits.switches = this.splitTrackBySwitches(clone(this.track));
    this.chooseBestPartsForTurbo(this.trackSplits.turns);
    this.willSwitchOn = null;
    this.willSwitchTo = false;
}

var TURN_START_DEGREES = 15;
var TURN_END_DEGREES = 25;

TrackInfo.prototype.splitTrackByTurns = function(origTrack) {
    var pieces = origTrack['pieces'];
    var track = [];
    var lastPiece = null;

    for (var i = 0; i < pieces.length; i++) {
        if (lastPiece === null || !this.arePiecesSame(pieces[i], lastPiece)) {
            if (lastPiece !== null) {
                lastPiece.next = pieces[i];
                track.push(lastPiece);
            }
            lastPiece = pieces[i];
            lastPiece.start = i;
            lastPiece.end = i;
            lastPiece.isTurn = this.isPieceTurn(lastPiece);
            if (!lastPiece.isTurn) {
                lastPiece.lengths = [lastPiece.length];
            }
            continue;
        }
        if (this.isPieceTurn(lastPiece)) {
            lastPiece.angle += pieces[i].angle;
        } else {
            lastPiece.length += pieces[i]['length'];
            lastPiece.lengths.push(pieces[i]['length']);
        }
        lastPiece.end = i;
    }
    track.push(lastPiece);

    if (this.arePiecesSame(track[0], track[track.length-1])) {
        var piece = track.pop();
        if (this.isPieceTurn(lastPiece)) {
            track[0].angle += piece.angle;
        } else {
            track[0].lengths = piece.lengths.concat(track[0].lengths);
            track[0].length += piece.length;
        }
        track[0]['start'] = piece['start'];
    }

    for (i = 0; i < track.length; i++) {
        track[i].smoothness = this.getTurnSmoothness(track[i]);
    }

    track[track.length-1].next = track[0];

    return track;
};

TrackInfo.prototype._countSwitchPartsLengths = function(track) {
    for (var i = 0; i < track.length; i++) {
        if (!track[i].hasOwnProperty('switch')) {
            continue;
        }
        track[i].switchesLenghts = this.getSwitchLengths(track[i]);
        var k = i + 1;
        if (k == track.length) {
            break;
        }
        var partLength = [];
        while (!track[k].hasOwnProperty('switch')) {
            for (var m = 0; m < track[k].laneLength.length; m++) {
                if ('undefined' == typeof partLength[m]) {
                    partLength[m] = 0;
                }
                partLength[m] += track[k].laneLength[m];
            }
            k++;
        }
        track[i].partLength = partLength;
    }
    if (track[track.length-1].hasOwnProperty('switch')) {
        partLength = [];
        k = 0;
        while (!track[k].hasOwnProperty('switch')) {
            for (m = 0; m < track[k].laneLength.length; m++) {
                if ('undefined' == typeof partLength[m]) {
                    partLength[m] = 0;
                }
                partLength[m] += track[k].laneLength[m];
            }
            k++;
        }
        track[track.length-1].partLength = partLength;
    }
    return track;
};

TrackInfo.prototype.splitTrackBySwitches = function(origTrack) {
    var pieces = origTrack['pieces'];
    var track;
    track = this._splitBySwitches(pieces);
    track = this._countSwitchPartsLengths(track);
    return track;
};

TrackInfo.prototype._splitBySwitches = function (pieces){
    var lastPiece = null;
    var track = [];

    for (var i = 0; i < pieces.length; i++) {
        pieces[i].laneLength = [];
        for (var j = 0; j < this.lanes.length; j++) {
            pieces[i].laneLength[this.lanes[j].index]
                = this.getLengthOfPiece(pieces[i], this.lanes[j].distanceFromCenter);
        }
        pieces[i].laneSmoothness = this.getTurnSmoothness(pieces[i]);
    }

    for (i = 0; i < pieces.length; i++) {
        if (lastPiece === null || !this.arePiecesSame(pieces[i], lastPiece)
            || pieces[i].hasOwnProperty('switch') || lastPiece.hasOwnProperty('switch')) {
            if (lastPiece !== null) {
                lastPiece.next = pieces[i];
                track.push(lastPiece);
            }
            lastPiece = pieces[i];
            lastPiece.start = i;
            lastPiece.end = i;
            lastPiece.isTurn = this.isPieceTurn(lastPiece);
            if (!lastPiece.isTurn) {
                lastPiece.lengths = [lastPiece.length];
            }
            continue;
        }
        for (var k = 0; k < pieces[i].laneLength.length; k++) {
            lastPiece.laneLength[k] += pieces[i].laneLength[k];
        }
        if (this.isPieceTurn(lastPiece)) {
            lastPiece.angle += pieces[i].angle;
        } else {
            lastPiece.length += pieces[i]['length'];
            lastPiece.lengths.push(pieces[i]['length']);
        }
        lastPiece.end = i;
    }
    track.push(lastPiece);

    if (this.arePiecesSame(track[0], track[track.length - 1])) {
        var piece = track.pop();
        if (this.isPieceTurn(lastPiece)) {
            track[0].angle += piece.angle;
        } else {
            track[0].lengths = piece.lengths.concat(track[0].lengths);
            track[0].length += piece.length;
        }
        for (k = 0; k < track[0].laneLength.length; k++) {
            track[0].laneLength[k] += piece.laneLength[k];
        }
        track[0]['start'] = piece['start'];
    }

    track[track.length-1].next = track[0];
    return track;
};

TrackInfo.prototype.chooseBestPartsForTurbo = function(trackSplit) {
    var longestStraight = {
        'length': 0
    };
    var piece;
    for (var i = 0; i < trackSplit.length; i++) {
        piece = trackSplit[i];
        if (!piece.isTurn && piece.length > longestStraight.length) {
            longestStraight = piece;
        }
    }
    longestStraight.turboPiece = true;
};

TrackInfo.prototype.getTurnSmoothness = function(piece) {
    var smoothness = [];
    for (var i = 0; i < this.lanes.length; i++) {
        if (piece.isTurn) {
            var angle = piece.angle < 0 ? -piece.angle : piece.angle;
            smoothness[this.lanes[i].index] = this.getLengthOfBendPiece(piece, this.lanes[i].distanceFromCenter) / angle;
        } else {
            smoothness[this.lanes[i].index] = 100;
        }
    }
    return smoothness;
};

TrackInfo.prototype.getLengthOfBendPiece = function (piece, laneDistance) {
    var radius = piece.radius;
    if (piece.angle > 0) {
        radius -= laneDistance;
    } else {
        radius += laneDistance;
    }
    var angle = piece.angle > 0 ? piece.angle : -piece.angle;
    var len = (2 * Math.PI * radius) * angle / 360;

    return len > 0 ? len : -len;
};

TrackInfo.prototype.getNextHardTurnDistance = function(piece, state) {
    return this.getNextTurnDistance(piece, state);
};

TrackInfo.prototype.getNextTurn = function(turnsPiece) {
    var current = turnsPiece;
    if (current.next.isTurn) {
        return current.next;
    }
    while(current.next.isTurn !== false) {
        current = current.next;
    }
    return current;
};

TrackInfo.prototype.getNextTurnDistance = function(piece, state){
    if (piece.isTurn && piece.inPieceDistance < piece.laneSmoothness*10) {
        return piece.laneSmoothness * TURN_START_DEGREES - piece.inPieceDistance;
    }
    return piece.length - this.getInSplitPieceDistance(piece, state);
};

TrackInfo.prototype.getInSplitPieceDistance = function(piece, state){
    var total = 0;
    var startPiece = state.pieceIndex;
    var endPiece = piece.end;
    var i;
    if (startPiece > endPiece) {
        for (i = startPiece; i < this.track.pieces.length ; i++) {
            total += this.getLengthOfPiece(this.track.pieces[i], this.lanes[state.lane].distanceFromCenter);
        }
        startPiece = 0;
    }
    for (i = startPiece; i <= piece.end; i++) {
        total += this.getLengthOfPiece(this.track.pieces[i], this.lanes[state.lane].distanceFromCenter);
    }
    total -= state.inPieceDistance;
    if (piece.hasOwnProperty('length')) {
        return piece.length - total;
    } else {
        return piece.laneLength[state.lane] - total;
    }
};

TrackInfo.prototype.getPiece = function(splitName, state) {
    if (this.willSwitchOn !== null && state.pieceIndex >= this.willSwitchOn.start) {
        this.willSwitchTo = false;
        this.willSwitchOn = null;
        console.log(this.isSwitching);
    }

    var originalPieceId = state.pieceIndex;
    var piece = null;
    for (var i = 0; i < this.trackSplits[splitName].length; i++) {
        if (originalPieceId >= this.trackSplits[splitName][i].start && originalPieceId <= this.trackSplits[splitName][i].end) {
            piece = this.trackSplits[splitName][i];
            break;
        }
    }
    if (piece === null) {
        piece = this.trackSplits[splitName][0];
    }
    return this._getSplitPieceInfo(splitName, piece, state);
};

TrackInfo.prototype.isPieceTurn = function(piece) {
    return 'undefined' === typeof piece['length'];
};

TrackInfo.prototype.arePiecesSame = function(piece1, piece2) {
    if (this.isPieceTurn(piece1) != this.isPieceTurn(piece2)) {
        return false;
    }
    if (!this.isPieceTurn(piece1)) {
        return true;
    }
    return piece1['radius'] == piece2['radius'] && piece1['angle'] * piece2['angle'] > 0;
};

TrackInfo.prototype.nextSwitchPieceIndex = function (currentPieceIndex) {
    //TODO to jest głupie, że za każdym razem w pętli leci, warto zoptymalizować
    for (var i = currentPieceIndex; i < this.track.pieces.length; i++) {
        var piece = this.track.pieces[i];
        if (piece.hasOwnProperty('switch') && piece.switch == true) {
            return i;
        }
    }

    return false;
};

TrackInfo.prototype.getLengthOfPiece = function (piece, laneDistance) {
    if (piece.hasOwnProperty('length')) {
        return piece.length;
    }
    return TrackInfo.prototype.getLengthOfBendPiece(piece, laneDistance);
};

TrackInfo.prototype._getSplitPieceInfo = function(splitName, trackPiece, state)
{
    var piece = clone(trackPiece);
    piece.inTurnZone = false;
    if ('undefined' != typeof piece.smoothness && piece.isTurn) {
        piece.length = this.getLengthOfBendPiece(piece, this.lanes[state.lane].distanceFromCenter);
        piece.laneSmoothness = piece.smoothness[state.lane];
    }

    piece.inPieceDistance = this.getInSplitPieceDistance(piece, state);
    piece.progress = piece.inPieceDistance / piece.length;
    if ('undefined' != typeof piece.smoothness && piece.isTurn) {
        piece.inTurnZone = piece.inPieceDistance > (piece.laneSmoothness * TURN_START_DEGREES);
        piece.outTurnZone = piece.length - piece.inPieceDistance < (piece.laneSmoothness * TURN_END_DEGREES);
    }

    if (trackPiece.end == 8) {
        console.log('ll: ' + piece.laneLength);
        console.log('ipn: ' +piece.inPieceDistance);
    }
    if ('undefined' !== typeof piece.laneLength) {

        piece.isLeaving =  piece.inPieceDistance +state.currentSpeed * 1.8 > 0;
    }
    return piece;
};

function clone(obj){
    if(obj == null || typeof(obj) != 'object')
        return obj;
    var newobj = obj.constructor();
    var hasNextReference = false;
    for(var key in obj) {
        if (key == 'next') {
            hasNextReference = true;
            continue;
        }
        newobj[key] = clone(obj[key]);
    }
    if (hasNextReference) {
        newobj.next = obj.next;
    }
    return newobj;
}

TrackInfo.prototype.getSwitchLengths = function(piece) {
    var res = [];
    for (var i = 0; i < this.lanes.length; i++) {
        res[i] = [];
        if ('undefined' !== typeof this.lanes[i-1]) {
            res[i][i-1] = this._getSwitchLength(piece, i, i-1);
        }
        if ('undefined' !== typeof this.lanes[i+1]) {
            res[i][i+1] = this._getSwitchLength(piece, i, i+1);
        }
    }
    return res;
};

TrackInfo.prototype._getSwitchLength = function(piece, startLane, endLane) {
    if (!piece.hasOwnProperty('angle')) {
        var laneDiff = this.lanes[startLane].distanceFromCenter - this.lanes[endLane].distanceFromCenter;

        return Math.sqrt(laneDiff*laneDiff + piece.length*piece.length);
    }
    var radius1 = piece.radius - this.lanes[startLane].distanceFromCenter;
    var radius2 = piece.radius - this.lanes[endLane].distanceFromCenter;
    return Math.PI * (1.5*(radius1 + radius2) - Math.sqrt(radius1*radius2)) * Math.abs(piece.angle) /360;
};

module.exports = TrackInfo;