function Driver(car, trackOptimalizator) {
    this.firstTick = true;
    this.car = car;
    this.trackOptimalizator = trackOptimalizator;
}

Driver.prototype.drive = function() {
    if (this.firstTick) {
        this.firstTick = false;
        return {
            msgType: "throttle",
            data: 1.0
        };
    }
    var turnsPiece = this.trackOptimalizator.trackInfo.getPiece('turns', this.car.state);
    var switchesPiece = this.trackOptimalizator.trackInfo.getPiece('switches', this.car.state);

        var switchTo = this.trackOptimalizator.getSuggestedSwitch(switchesPiece, this.car.state);
        if (switchTo !== false) {
            return {
                msgType: "switchLane",
                data: switchTo
            }
        }
    var res = this.car.getThrottle(turnsPiece);
//        console.log(res);
    if (res == 'turbo') {
        return {
            msgType: "turbo",
            data: ''
        };
    }
    return {
        msgType: "throttle",
        data: res
    };
};

module.exports = Driver;