function TurnOptimalizator(predictor)
{
    this.trackInfo = null;
    this.predictor = predictor;
    this.prevState = null;
    this.state = null;
    this.prevAngle = null;
    this.willSwitchOn = null;
    this.willSwitchTo = false;

//    this.data =
//   { '157': 3.993918372059483,
//  '191': 3.88029772955381,
//  '331': 3.141585068260291,
//  '366': 3.01322074932331 };
    this.data = { };
}

var TOO_FAST = 0.1;
var OOPS = 0;
var FINE = 0.7;
var A_BIT_SLOWER_PLZ = 0.5;

var OPTIMAL_ANGLE = 45;
var UNKNOWN_TURN_SPEED = 1;

TurnOptimalizator.prototype.getThrottle = function (turnPiece, state) {
    var angleDiff = this.getAngleDiff(state.angle);
    var throttle = this.react(turnPiece, state, angleDiff);
    //console.log('ps: ' + turnPiece.start + ', ls: '+turnPiece.laneSmoothness + ', angle:' + state.angle + ', throt: ' + throttle + ', angDiff: ' + angleDiff);
    this.addData(turnPiece.laneSmoothness, state.currentSpeed, state.angle, angleDiff);
    return throttle;
};

TurnOptimalizator.prototype.addData = function (laneSmoothness, speed, angle, angleDiff) {
    laneSmoothness = smoothnessToInt(laneSmoothness);
    angle = Math.abs(angle);

    if (Math.abs(angleDiff) > 1.7 || speed == 0) {
        return;
    }

    if (angle < OPTIMAL_ANGLE) {
        //bug - nadpisywana jest wieksza predkosc w pewnej sytuacji
        if ('undefined' == typeof this.data[laneSmoothness]) {
            for(var key in this.data) {
                if (key < laneSmoothness && this.data[key] > speed) {
                    speed = this.data[key];
                }
            }
            this.data[laneSmoothness] = speed;
        }
        if (this.data[laneSmoothness] < speed) {
            for(var key in this.data) {
                if (key > laneSmoothness) {
                    this.data[key] = speed;
                }
            }
            this.data[laneSmoothness] = speed;
        }
    }
};

TurnOptimalizator.prototype.getSpeedForTurn = function(turnsPiece, lane) {
    if (!turnsPiece.isTurn) {
        console.log('TO NIE ZAKRET KURWA');
    }
    if (this.trackInfo.willSwitchTo != false) {
        if (turnsPiece.start >= this.trackInfo.willSwitchOn.start) {
            lane = this.trackInfo.willSwitchTo;
        }
    }
    console.log('\\\\\\');
    var smoothness = smoothnessToInt(turnsPiece.smoothness[lane]);
    console.log('smth:' + smoothness);
    console.log('//////');
    if ('undefined' == typeof this.data[smoothness]) {
        for (var key in this.data) {
            if (key < smoothness) {
                return this.data[key];
            }
        }
        return UNKNOWN_TURN_SPEED;
    } else {
        return this.data[smoothness];
    }
};

TurnOptimalizator.prototype.react = function(turnPiece, state, angleDiff)
{
    var angle = Math.abs(state.angle);
    if (angle > OPTIMAL_ANGLE) {
        return OOPS;
    } else {
        if (angleDiff > 2.0) {
            return TOO_FAST;
        }
        if (angleDiff < -4.0) {
            return A_BIT_SLOWER_PLZ;
        }
        return FINE;
    }
};

TurnOptimalizator.prototype.throttle = function(piece, state) {
    this.prevState = {
        'angle': Math.abs(state.angle)
    }
};

TurnOptimalizator.prototype.getAngleDiff = function(angle) {
    angle = Math.abs(angle);
    if (this.prevAngle == null) {
        this.prevAngle = angle;
        return 0;
    }
    var res = angle -  this.prevAngle;
    this.prevAngle = angle;
    return res;
};

function smoothnessToInt(value)
{
    value *= 100;
    return ~~value;
}


module.exports = TurnOptimalizator;
