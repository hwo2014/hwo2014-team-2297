function TrackOptimalizator() {
    this.trackInfo = null;
    this.predictor = null;

    this.lastPiece = null;
    this.a = null;
    this.switch = {
        'piece': null,
        'side': null
    }
}

TrackOptimalizator.prototype.getSuggestedSwitch = function (piece, state) {
    var prev;
    for (var i = 0; i < this.trackInfo.trackSplits.switches.length; i++) {
        prev = piece;
        piece = piece.next;
        if ('undefined' !== typeof piece.switch) {
            break;
        }
    }

    this.trackInfo.willSwitchTo = this.getBestLaneIndex(piece, state);
    if (this.willSwitchTo === false) {
        this.trackInfo.willSwitchOn = null;
    } else {
        this.trackInfo.willSwitchOn = piece;
    }
    if (this.lastPiece === null || piece.start != this.lastPiece.start) {
        console.log(piece.start);
        console.log('wst: ', this.trackInfo.willSwitchTo);
        console.log();
        this.lastPiece = piece;
        if ('undefined' !== typeof piece.switch) {
        }
    }
    piece = this.trackInfo._getSplitPieceInfo('', piece, state);
    if (piece.isLeaving) {
        var res = this.laneToDirection(state.lane, this.trackInfo.willSwitchTo);
        if (this.switch.piece !== state.pieceIndex || this.switch.side !== res) {
            this.switch.piece = state.pieceIndex;
            this.switch.side = res;
            console.log('a: ' + this.switch.side);
            return res;
        } else {
            this.switch.piece = state.pieceIndex;
            this.switch.side = res;
            console.log('b: ' + this.switch.side);
            return false
        }
    }

    return false;
};

TrackOptimalizator.prototype.getBestLaneIndex = function (piece, state) {
    var shortest = piece.partLength[0];
    var index = 0;
    for (var i = 1; i < piece.partLength.length; i++) {
        if (piece.partLength[i] < shortest ||  piece.partLength[i] == shortest && state.lane == i) {
            if (state.lane == i || this.isSwitchPossible(piece, state.lane, i)) {
                index = i;
                shortest = piece.partLength[i];

            }
        }
    }
    if (index == state.currentLane) {
        return false;
    }
    return index;
};

TrackOptimalizator.prototype.isSwitchPossible = function (piece, startLane, currentLane) {

    return true;
};

TrackOptimalizator.prototype.laneToDirection = function (start, end) {
    if (start < end) {
        return 'Right';
    } else if (start > end){
        return 'Left';
    }
    return false;
};

module.exports = TrackOptimalizator;