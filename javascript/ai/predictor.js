function Predictor()
{
    this.params = {};
    this.datasets = {};

    this.datasets['va'] = [0.58808, 0.7763184, 0.960792032, 1.1415761914, 1.3187446675, 1.4923697742, 1.6625223787, 1.8292719311, 1.9926864925, 2.1528327627, 2.3097761074, 2.4635805853, 2.6143089735, 2.7620227941, 2.9067823382, 3.0486466914, 3.1876737576, 3.3239202824, 3.4574418768, 3.5882930393, 3.7165271785, 3.8421966349, 3.9653527022, 4.0860456482, 4.2043247352, 4.3202382405, 4.4338334757, 4.5451568062, 4.6542536701, 4.7611685967, 4.8659452247, 4.9686263202, 5.0692537938, 5.1678687179, 5.2645113436, 5.3592211167, 5.4520366944, 5.5429959605, 5.6321360413, 5.7194933205, 5.805103454, 5.889001385, 5.9712213573, 6.0517969301, 6.1307609915, 6.2081457717 ];
    this.datasets['vb'] = [6.3583031991, 6.2311371351, 6.1065143924, 5.9843841046, 5.8646964225, 5.7474024941, 5.6324544442, 5.5198053553, 5.4094092482, 5.3012210632, 5.195196642, 5.0912927091, 4.9894668549, 4.8896775178, 4.7918839675, 4.6960462881, 4.6021253624, 4.5100828551, 4.419881198, 4.3314835741, 4.2448539026, 4.1599568245, 4.076757688, 3.9952225343, 3.9153180836, 3.8370117219, 3.7602714875, 3.6850660577, 3.6113647366, 3.5391374418, 3.468354693, 3.3989875991, 3.3310078472, 3.2643876902, 3.1990999364, 3.1351179377, 3.0724155789, 3.0109672674, 2.950747922, 2.8917329636, 2.8338983043, 2.7772203382, 2.7216759314, 2.6672424128, 2.6138975646, 2.5616196133, 2.510387221, 2.4601794766, 2.4109758871, 2.3627563693, 2.3155012419, 2.2691912171, 2.2238073927, 2.1793312449, 2.13574462, 2.0930297276, 2.051169133, 2.0101457504, 1.9699428354, 1.9305439787, 1.8919330991, 1.8540944371, 1.8170125484, 1.7806722974, 1.7450588515, 1.7101576744, 1.6759545209, 1.6424354305, 1.6095867219, 1.5773949875, 1.5458470877, 1.514930146, 1.484631543, 1.4549389122, 1.4258401339, 1.3973233313, 1.3693768646, 1.3419893273, 1.3151495408, 1.28884655, 1.263069619, 1.2378082266, 1.2130520621, 1.1887910208, 1.1650152004];
}

Predictor.prototype.predictBraking = function(speed, expected)
{
    if (speed <= expected) {
        return 0;
    }
    if (this.datasets['vb'].length < 10) {
        return false;
    }
    var s = this._findPoint('vb', speed);
    var f = this._findPoint('vb', expected);
    var diff = 1;
    if (s > f) {
        diff = -1;
    }
    var sum = 0;
    for (var i = s; i <= f; i += diff) {
        sum += this.predict('vb', i);
    }
    return sum;
};

Predictor.prototype.predict = function (datasetName, x) {
    if (this.datasets[datasetName].length < 10) {
        return false;
    }
    var points = this._getPoints(this.datasets[datasetName]);
    var params = this._getParams(points);
    var valueL = this._getValueL(points, x);
    var valueQ = this._getValue(params, x);
    return (valueL + valueQ) * 0.5;
};

Predictor.prototype._findPoint = function (datasetName, value) {
    var diff = 1;
    var a = this.predict(datasetName, 0);
    var b = this.predict(datasetName, 1);
    if (value > a && a > b) {
        diff = -1;
    }
    if (value < a && a < b) {
        diff = -1;
    }
    var x = 0;
    var val = null;
    for(var i = 0; i < 300; i++) {
        val =  this.predict(datasetName, x += diff);
        if (value > val && diff > 0) {
            return x;
        }
        if (value < val && diff < 0) {
            return x;
        }
    }
    return false;
};

Predictor.prototype._getPoints = function (data) {
    var last = data.length - 1;
    var mid = ~~(last * 0.9);
    if (mid == last) {
        mid -= 1;
    }
    return [
        [0, data[0]],
        [mid, data[mid]],
        [last, data[last]]
    ];
};

Predictor.prototype._getValueL = function(points, x) {
    var a = points[0];
    var b = points[2];
    return a[1] + ((b[1] - a[1]) / (b[0] - a[0])) * (x-a[0]);

}

Predictor.prototype._getParams = function (points) {
    var a = points[0];
    var b = points[1];
    var c = points[2];

    var res = [];
    res[0] = a[1] / ((a[0] - b[0]) * (a[0] - c[0]))
        + b[1] / ((b[0] - a[0]) * (b[0] - c[0]))
        + c[1] / ((c[0] - a[0]) * (c[0] - b[0]));
    res[1] = -a[1] * (b[0] + c[0]) / ((a[0] - b[0]) * (a[0] - c[0]))
        - b[1] * (a[0] + c[0]) / ((b[0] - a[0]) * (b[0] - c[0]))
        - c[1] * (a[0] + b[0]) / ((c[0] - a[0]) * (c[0] - b[0]));
    res[2] = a[1] * b[0] * c[0] / ((a[0] - b[0]) * (a[0] - c[0]))
        + b[1] * a[0] * c[0] / ((b[0] - a[0]) * (b[0] - c[0]))
        + c[1] * a[0] * b[0] / ((c[0] - a[0]) * (c[0] - b[0]));
    return res;
};

Predictor.prototype._getValue = function (params, x) {
    return (params[0] * (x * x) + params[1] * x + params[2]);
};

module.exports = Predictor;
