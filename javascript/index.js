
var net = require("net");
var fs = require("fs");
var JSONStream = require('JSONStream');

var Car = require('./ai/car.js');
var TrackOptimalizator = require('./ai/track_optimalizator.js');
var TrackInfo = require('./ai/track_info.js');
var Driver = require('./ai/driver.js');
var Predictor = require('./ai/predictor.js');
var TurnOptimalizator = require('./ai/turn_optimalizator.js');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var trackName = process.argv[6];
var carCount = process.argv[7];

var car;
var trackOptimalizator;
var trackInfo;
var driver;

var predictor = new Predictor;

client = net.connect(serverPort, serverHost, function () {

    if (!trackName) {
        return send({
            msgType: "join",
            data: {
                name: botName,
                key: botKey
            }
        });
    } else {
        if (!carCount) {
            carCount = 1;
        }

        return send({
            msgType: "joinRace",
            data: {
                botId: {
                    name: botName,
                    key: botKey
                },
                trackName: trackName,
                carCount: carCount
            }
        });        
    }
});

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
}


jsonStream = client.pipe(JSONStream.parse());

function drawDots(data) {
    if ('undefined' != data.gameTick && data.gameTick % 100 == 0) {
        console.log('tick: ' + data.gameTick);
    }
}


jsonStream.on('data', function (data) {

    switch (data.msgType) {
        case 'carPositions':
            drawDots(data);
            var command = {
                msgType: "throttle",
                data: 1.0
            };

            var tick = 'undefied' == typeof data.gameTick ? 0 : data.gameTick;
            car.update(data.data, tick);
            if ('undefined' != typeof driver) {
                 command = driver.drive();
            }
            send(command);
            break;
        case 'join':
            console.log('Joined');
            break;
        case 'yourCar':
            car = new Car(data.data, predictor);
            console.log('Your car is %s', data.data.color);
            break;
        case 'gameInit':
            trackOptimalizator = new TrackOptimalizator(data.data.race.track, predictor);
            trackInfo = new TrackInfo(data.data.race.track, data.data.race.raceSession.laps);
            car.trackInfo = trackInfo;
            car.turnOptimizer = new TurnOptimalizator(predictor);
            car.turnOptimizer.trackInfo = trackInfo;
            trackOptimalizator.trackInfo = trackInfo;
            driver = new Driver(car, trackOptimalizator);

            console.log('Race prepared: %d laps', data.data.race.raceSession.laps);
            // fs.writeFile('logs/gameInit.json', JSON.stringify(data, null, 4), function(err) {});
            break;
        case 'gameStart':
            console.log('Race started');
            break;
        case 'gameEnd':
            console.log('Race ended');
            break;
        case 'crash':
            console.log('CRSH!');
            break;
        case 'turboAvailable':
            car.turboTicks = data.data.turboDurationTicks;
            car.turboFactor = data.data.turboFactor;
            car.turboEnd = data.gameTick + car.turboTicks;
            car.turboAvailable = true;
            break;
        case 'lapFinished':
            car.lapFinished(data.data);

            console.log('Lap time: %s', data.data.lapTime.millis);
            break;
        case 'dnf':
            console.log('DNF wooooo! [%s]', data.reason);
            break;
        default:
            send({
                msgType: "ping",
                data: {}
            });
            break;
    }
});

jsonStream.on('error', function () {
    return console.log("disconnected");
});
